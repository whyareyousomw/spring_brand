import com.goods.demo.pojo.Brand;
import com.goods.demo.pojo.PageBean;
import com.goods.demo.service.BrandService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(
        locations = {"classpath:applicationContext_mapper.xml",
                "classpath:applicationContext_service.xml"})
public class Tests {

    @Autowired
    BrandService service;


    @Test
    public void selectAllBrand(){
        List<Brand> brandList = service.brandList();
        for (Brand bra:brandList) {
            System.out.println(bra);
        }

    }

    @Test
    public void  addNew(){
        Brand brand = new Brand("电饭锅","电饭锅技222",44,"电饭锅，看的见得颜值",1);
        int i = service.addBrand(brand);

        System.out.println(i);
    }

    @Test
    public void  deleteByIds(){
        int[] ids = {54,55};
        int i = service.deleteByIds(ids);

        System.out.println(i>0?"success":"erroe");
    }


    @Test
    public void queryBrands(){
        List<Brand> brandList = service.queryBrand(0,"","");
        for (Brand bra:brandList) {
            System.out.println(bra);
        }

    }

    @Test
    public void selectByPage(){
        PageBean<Brand> brandList = service.selectByPage(2,5);
        List<Brand> brands = brandList.getRows();
        System.out.println(brandList.getTotalCount());
        for (Brand bra: brands) {
            System.out.println(bra);
        }

    }

}
