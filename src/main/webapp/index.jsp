<%--
  Created by IntelliJ IDEA.
  User: user
  Date: 2022-12-09
  Time: 16:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>商品</title>
    <style>
        .el-table .warning-row {
            background: oldlace;
        }

        .el-table .success-row {
            background: #f0f9eb;
        }
    </style>
</head>
<body>
<div id="myapp">
    <el-form :inline="true" :model="brand" class="demo-form-inline">
        <el-form-item label="当前状态">
            <el-select v-model="brand.status" placeholder="当前状态">
                <el-option label="禁用" value="0"></el-option>
                <el-option label="启用" value="1"></el-option>
            </el-select>
        </el-form-item>
        <el-form-item label="企业名称">
            <el-input v-model="brand.company_name" placeholder="企业名称"></el-input>
        </el-form-item>
        <el-form-item label="品牌名称">
            <el-input v-model="brand.brand_name" placeholder="品牌名称"></el-input>
        </el-form-item>
        <el-form-item>
            <el-button type="primary" @click="onSubmit">查询</el-button>
        </el-form-item>
    </el-form>

    <el-row>
        <el-button type="danger" @click="deleteByIds" plain>批量删除</el-button>
        <el-button type="primary" @click="dialogFormVisible = true" plain>新增</el-button>
    </el-row>
    <br>

    <el-table
            ref="multipleTable"
            :data="tableData"
            tooltip-effect="dark"
            style="width: 100%"
            @selection-change="handleSelectionChange"
            :row-class-name="tableRowClassName">
        <el-table-column
                type="selection"
                width="55">
        </el-table-column>
        <el-table-column
                prop="brand_name"
                label="品牌名称"
                align="center">
        </el-table-column>
        <el-table-column
                prop="company_name"
                label="企业名称"
                align="center">
        </el-table-column>
        <el-table-column
                prop="ordered"
                label="排序"
                align="center">
        </el-table-column>
        <el-table-column
                prop="statusStr"
                label="当前状态"
                align="center">
        </el-table-column>
        <el-table-column label="操作" align="center">
            <template slot-scope="scope">
                <el-button
                        size="mini"
                        @click="handleEdit(scope.$index, scope.row) ">修改
                </el-button>
                <el-button
                        size="mini"
                        type="danger"
                        @click="handleDelete(scope.$index, scope.row)">删除
                </el-button>
            </template>
        </el-table-column>


    </el-table>
    <el-pagination
            @size-change="handleSizeChange"
            @current-change="handleCurrentChange"
            :current-page="currentPage"
            :page-sizes="[8, 16, 24, 32]"
            :page-size="8"
            layout="total, sizes, prev, pager, next, jumper"
            :total="totalCount">
    </el-pagination>


    <el-dialog title="新增品牌" :visible.sync="dialogFormVisible" width="30%">
        <el-form :model="addbrand" label-width="100px">
            <el-form-item label="品牌名称" label-width="80px">
                <el-input v-model="addbrand.brand_name"></el-input>
            </el-form-item>
            <el-form-item label="企业名称" label-width="80px">
                <el-input v-model="addbrand.company_name"></el-input>
            </el-form-item>
            <el-form-item label="排序" label-width="80px">
                <el-input v-model="addbrand.ordered"></el-input>
            </el-form-item>
            <el-form-item label="备注" label-width="80px">
                <el-input type="textarea" :rows="2" placeholder="请输入内容" v-model="addbrand.description"></el-input>
            </el-form-item>
            <el-form-item label="状态" prop="status" label-width="80px">
                <el-switch v-model="addbrand.status"
                           active-value="1"
                           inactive-value="0"></el-switch>
            </el-form-item>

        </el-form>

        <div slot="footer" class="dialog-footer">
            <el-button @click="dialogFormVisible = false">取 消</el-button>
            <el-button type="primary" @click="addBrand">确 定</el-button>
        </div>
    </el-dialog>

    <el-dialog title="修改品牌" :visible.sync="dialogFormVisibleEdit" width="30%">
        <el-form :model="addbrand" label-width="100px">
            <el-form-item label="品牌名称" label-width="80px">
                <el-input v-model="editbrand.brand_name"></el-input>
            </el-form-item>
            <el-form-item label="企业名称" label-width="80px">
                <el-input v-model="editbrand.company_name"></el-input>
            </el-form-item>
            <el-form-item label="排序" label-width="80px">
                <el-input v-model="editbrand.ordered"></el-input>
            </el-form-item>
            <el-form-item label="备注" label-width="80px">
                <el-input type="textarea" :rows="2" v-model="editbrand.description"></el-input>
            </el-form-item>
            <el-form-item label="状态" prop="status" label-width="80px">
                <el-switch v-model="editbrand.status"
                           active-value="1"
                           inactive-value="0"></el-switch>
            </el-form-item>

        </el-form>

        <div slot="footer" class="dialog-footer">
            <el-button @click="dialogFormVisibleEdit = false">取 消</el-button>
            <el-button type="primary" @click="editBrand">确 定</el-button>
        </div>
    </el-dialog>


</div>

</body>
<script src="js/axios-0.18.0.js"></script>
<script src="js/vue.js"></script>
<script src="element-ui/lib/index.js"></script>
<link rel="stylesheet" href="element-ui/lib/theme-chalk/index.css">
<script>
    new Vue({
        el: "#myapp",
        mounted() {
            // mounted vue已完成初始化和页面加载完成，发送异步请求，查询数据
            this.selectByPageAndCondition();
        },
        data() {
            return {
                brand: {
                    brand_name: '',
                    company_name: '',
                    ordered: '',
                    description: '',
                    status: ''
                },
                addbrand: {
                    brand_name: '',
                    company_name: '',
                    ordered: '',
                    description: '',
                    status: ''

                },
                editbrand: {
                    id: '',
                    brand_name: '',
                    company_name: '',
                    ordered: '',
                    description: '',
                    status: ''

                },
                // 表格数据
                tableData: [],
                // 总记录数  初始100
                totalCount: 100,
                // 每页显示条数
                pageSize: 8,
                // 分页当前页
                currentPage: 1,
                // 复选框选中数据集合
                multipleSelection: [],
                // 被选中的id数组
                selectedIds: [],
                dialogFormVisible: false,
                dialogFormVisibleEdit: false,
            }
        },
        methods: {
            tableRowClassName({row, rowIndex}) {
                if (rowIndex % 3 == 0) {
                    return 'warning-row';
                } else if (rowIndex % 3 == 1) {
                    return 'success-row';
                }
                return '';
            },

            onSubmit() {

                axios({
                    method: "post",
                    url: "http://localhost:8080/queryBrand",
                    data: _this.brand,

                }).then(resp => {
                    //设置表格数据
                    this.tableData = resp.data;
                    //设置总记录数
                    this.totalCount = resp.data.totalCount;
                    this.selectByPageAndCondition();

                })

                console.log(_this.brand);
            },
            toggleSelection(rows) {
                if (rows) {
                    rows.forEach(row => {
                        this.$refs.multipleTable.toggleRowSelection(row);
                    });
                } else {
                    this.$refs.multipleTable.clearSelection();
                }
            },
            handleSelectionChange(val) {
                this.multipleSelection = val;

            },
            // 分页多条件查询
            selectByPageAndCondition() {
                var _this = this;   //搭桥小操作
                axios({
                    method: "post",
                    url: "http://localhost:8080/selectByPage?currentPage=" + _this.currentPage + "&pageSize=" + _this.pageSize,
                }).then(function (resp) {
                    //this.tableData = resp.data  这样无法设置，因为此处的this是axios的，要使用Vue的this要进行搭桥小操作
                    //设置表格数据
                    _this.tableData = resp.data.rows;   //{rows:[], totalCount:100}
                    console.log(resp.data);
                    //设置总记录数
                    _this.totalCount = resp.data.totalCount;
                })
            },
            handleSizeChange(val) {
                console.log(`每页 ${val} 条`);
                this.pageSize = val;
                this.selectByPageAndCondition();
            },
            handleCurrentChange(val) {
                console.log(`当前页: ${val}`);
                this.currentPage = val;
                this.selectByPageAndCondition();
            },
            // 查询所有
            brandList() {
                var _this = this;   //搭桥小操作
                axios({
                    method: "get",
                    url: "http://localhost:8080/brandList"
                }).then(function (resp) {
                    //this.tableData = resp.data  这样无法设置，因为此处的this是axios的，要使用Vue的this要进行搭桥小操作
                    //设置表格数据
                    _this.tableData = resp.data;
                    console.log("撒娇娇的撒娇");
                })
            },
            addBrand() {
                var _this = this;   //搭桥小操作
                console.log(_this.addbrand)
                axios({
                    method: "post",
                    url: "http://localhost:8080/addBrand",
                    data: _this.addbrand
                }).then(function (resp) {
                    _this.dialogFormVisible = false
                    //设置表格数据
                    _this.selectByPageAndCondition()
                    _this.$message({
                        message: '恭喜你！添加成功',
                        type: 'success',
                        duration: 1000
                    })
                    console.log(_this.addbrand);
                })
            },
            deleteByIds() {
                this.$confirm('此操作将删除该数据, 是否继续?', '提示', {
                    confirmButtonText: '确定',
                    cancelButtonText: '取消',
                    type: 'warning'
                }).then(() => {
                    for (let i = 0; i < this.multipleSelection.length; i++) {
                        let selectionElement = this.multipleSelection[i];
                        this.selectedIds[i] = selectionElement.id;
                    }
                    var _this = this;   //搭桥小操作
                    axios({
                        method: "post",
                        url: "http://localhost:8080/deleteByIds",
                        data: _this.selectedIds
                    }).then(function (resp) {
                        _this.selectByPageAndCondition();
                        _this.message({
                            message: '恭喜你！删除成功',
                            type: 'success',
                            duration: 500
                        });
                    })
                }).catch(() => {
                    //用户点击取消按钮
                    this.$message({
                        type: 'info',
                        message: '已取消删除'
                    });
                })
            },
            handleEdit(index, row) {
                console.log(index, row);
                this.dialogFormVisibleEdit = true
                this.editbrand.id = row.id;
                this.editbrand.brand_name = row.brand_name;
                this.editbrand.company_name = row.company_name;
                this.editbrand.ordered = row.ordered;
                this.editbrand.description = row.description;
                this.editbrand.status = row.status;

            },
            editBrand() {

                axios({
                    method: "post",
                    url: "http://localhost:8080/editBrand",
                    data: _this.editbrand
                }).then(resp => {
                    this.dialogFormVisibleEdit = false
                    //设置表格数据
                    this.selectByPageAndCondition()
                    this.$message({
                        message: '恭喜你！修改成功',
                        type: 'success',
                        duration: 1000
                    })
                    console.log(_this.editbrand);
                })
            },
            handleDelete(index, row) {
                var _this = this;   //搭桥小操作
                console.log(index, row)
                console.log(row.id)
                axios({
                    method: "get",
                    url: "http://localhost:8080/deleteById",
                    params: {
                        id: row.id
                    }
                }).then(function (resp) {
                    //设置表格数据
                    _this.selectByPageAndCondition();
                    _this.$message({
                        message: '恭喜你！删除成功',
                        type: 'success',
                        duration: 500
                    });
                })
            }
        }

    })

</script>


</html>