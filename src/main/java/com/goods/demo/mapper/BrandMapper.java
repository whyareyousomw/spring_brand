package com.goods.demo.mapper;

import com.goods.demo.pojo.Brand;
import com.goods.demo.pojo.PageBean;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface BrandMapper {
    //查询所有
     List<Brand> brandList();
    //新增品牌
     int addBrand(Brand brand);
     //条件查询
    List<Brand> queryBrand (@Param("status") int status,
                            @Param("brand_name") String brand_name,
                            @Param("company_name") String company_name);


    /**
     * 分页查询
     * @param begin
     * @param size
     * @return
     */

    List<Brand> selectByPage(@Param("begin") int begin, @Param("size") int size);
    /**
     * 查询记录数
     * @return
     */
    int selectTotalCount();
    /**
     * 分页多条件查询
     * @param begin
     * @param size
     * @return
     */
    PageBean<Brand> selectByPageAndCondition(@Param("begin") int begin,@Param("size") int size);


    int deleteByIds(@Param("ids") int[] ids);

    int deleteById(int id);

    int editBrand(Brand brand);

}
