package com.goods.demo.service;

import com.goods.demo.pojo.Brand;
import com.goods.demo.pojo.PageBean;

import java.util.List;

public interface BrandService {

    //查询所有
     List<Brand> brandList();
    //新增品牌
     int addBrand(Brand brand);
    //条件查询
    List<Brand> queryBrand (int status,String brand_name,String company_name);
    /**
     * 分页查询
     * @param currentPage
     * @param pageSize
     * @return
     */
    PageBean<Brand> selectByPage(int currentPage, int pageSize);

    /**
     * 分页多条件查询
     * @param currentPage
     * @param pageSize
     * @return
     */
    PageBean<Brand> selectByPageAndCondition(int currentPage, int pageSize);

    int deleteByIds(int[] ids);

    int deleteById(int id);

    int editBrand(Brand brand);


}
