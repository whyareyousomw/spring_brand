package com.goods.demo.service;

import com.goods.demo.mapper.BrandMapper;
import com.goods.demo.pojo.Brand;
import com.goods.demo.pojo.PageBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BrandServiceImp implements BrandService{

    @Autowired
    BrandMapper brandMapper;

    @Override
    public List<Brand> brandList() {
        return brandMapper.brandList();
    }

    @Override
    public int addBrand(Brand brand) {
        return brandMapper.addBrand(brand);
    }

    @Override
    public List<Brand> queryBrand(int status, String brand_name, String company_name) {
        return brandMapper.queryBrand(status,brand_name,company_name);
    }

    @Override
    public PageBean<Brand> selectByPage(int currentPage, int pageSize) {
        //4. 计算开始索引
        int begin = (currentPage - 1) * pageSize;
        // 计算查询条目数
        int size = pageSize;
        //5. 调用方法
        List<Brand> rows = brandMapper.selectByPage(begin, size);

        //6. 查询总记录数
        int totalCount = brandMapper.selectTotalCount();

        //7. 封装pageBean对象
        PageBean<Brand> pageBean = new PageBean<Brand>();
        pageBean.setRows(rows);
        pageBean.setTotalCount(totalCount);
        return pageBean;
    }

    @Override
    public PageBean<Brand> selectByPageAndCondition(int currentPage, int pageSize) {
        return brandMapper.selectByPageAndCondition(currentPage,pageSize);
    }

    @Override
    public int deleteByIds(int[] ids) {
        return brandMapper.deleteByIds(ids);
    }

    @Override
    public int deleteById(int id) {
        return brandMapper.deleteById(id);
    }

    @Override
    public int editBrand(Brand brand) {
        return brandMapper.editBrand(brand);
    }
}
