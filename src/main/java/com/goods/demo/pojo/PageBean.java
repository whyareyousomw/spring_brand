package com.goods.demo.pojo;

import java.util.List;

// 分页查询的JavaBean  （<T> new对象的时候传进来）
public class PageBean<T> {

    // 总记录数
    private int totalCount;
    // 当前页数据 （T为自定义泛型，因为以后可能传递的是Brand或者User 代码复用）
    private List<T> rows;

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }
}
