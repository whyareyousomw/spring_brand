package com.goods.demo.controller;

import com.alibaba.fastjson.JSON;
import com.goods.demo.pojo.Brand;
import com.goods.demo.pojo.PageBean;
import com.goods.demo.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;


@RestController
public class BrandController {

    @Autowired
    BrandService brandService;

    @RequestMapping("/brandList")
    public void brandList(HttpServletResponse resp) throws IOException {
        List<Brand> brandList = brandService.brandList();
        //2. 转为json
        String jsonString = JSON.toJSONString(brandList);
        resp.setContentType("text/json;charset=utf-8"); //处理中文乱码
        resp.getWriter().write(jsonString);
    }

    @RequestMapping("/addBrand")
    public void addBrand(HttpServletRequest request,HttpServletResponse resp) throws IOException {
        BufferedReader br = request.getReader();
        String params = br.readLine();
        Brand brand = JSON.parseObject(params,Brand.class);
        System.out.println(brand);
        int i = brandService.addBrand(brand);
        resp.getWriter().write(i);
    }

    @RequestMapping("/editBrand")
    public void editBrand (HttpServletRequest request,HttpServletResponse resp) throws IOException {
        BufferedReader br = request.getReader();
        String params = br.readLine();
        Brand brand = JSON.parseObject(params,Brand.class);
        System.out.println(brand);
        int i = brandService.editBrand(brand);
        resp.getWriter().write(i);
    }

    @RequestMapping("/deleteById")
    public void deleteById(int id,HttpServletRequest request,HttpServletResponse resp) throws IOException {
        System.out.println(id);
        int i = brandService.deleteById(id);
        System.out.println(i);
        resp.getWriter().write(i);
    }

    @RequestMapping("/deleteByIds")
    public void deleteByIds(HttpServletRequest request,HttpServletResponse resp) throws IOException {
        BufferedReader br = request.getReader();
        String params = br.readLine();
        int[] ids = JSON.parseObject(params,int[].class);
        System.out.println(request.getRequestURI()+"---"+request.getQueryString());
        int i = brandService.deleteByIds(ids);
        resp.getWriter().write(i);
    }



    @RequestMapping("/queryBrand")
    public void queryBrand(HttpServletRequest request, HttpServletResponse resp) throws IOException {
        BufferedReader br = request.getReader();
        String params = br.readLine();
        Brand brand = JSON.parseObject(params,Brand.class);
        System.out.println(brand+"---"+request.getRequestURI()+"---"+request.getQueryString());
        List<Brand> brandList = brandService.queryBrand(brand.getStatus(),brand.getBrand_name(), brand.getCompany_name());
        //2. 转为json
        String jsonString = JSON.toJSONString(brandList);
        resp.getWriter().write(jsonString);
    }
    @RequestMapping("/selectByPage")
    public void selectByPage(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //1. 接收 当前页码 和 每页展示条数    get提交  url?currentPage=1&pageSize=5
        String _currentPage = req.getParameter("currentPage");
        String _pageSize = req.getParameter("pageSize");

        int currentPage = Integer.parseInt(_currentPage);
        int pageSize = Integer.parseInt(_pageSize);



        //2. 调用service查询
        PageBean<Brand> pageBean = brandService.selectByPage(currentPage, pageSize);

        //3. 转为json
        String jsonString = JSON.toJSONString(pageBean);

        //3. 响应数据
        resp.setContentType("text/json;charset=utf-8"); //处理中文乱码
        resp.getWriter().write(jsonString);
    }



    @RequestMapping("/selectByPageAndCondition")
    public void selectByPageAndCondition(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        //1. 接收 当前页码 和 每页展示条数    get提交  url?currentPage=1&pageSize=5
        String _currentPage = req.getParameter("currentPage");
        String _pageSize = req.getParameter("pageSize");

        int currentPage = Integer.parseInt(_currentPage);
        int pageSize = Integer.parseInt(_pageSize);

        // 接收brand数据
        BufferedReader br = req.getReader();
        String params = br.readLine();

        //转为Brand对象
        Brand brand = JSON.parseObject(params, Brand.class);

        //2. 调用service查询
        PageBean<Brand> pageBean = brandService.selectByPageAndCondition(currentPage, pageSize);

        //3. 转为json
        String jsonString = JSON.toJSONString(pageBean);

        //3. 响应数据
        resp.setContentType("text/json;charset=utf-8"); //处理中文乱码
        resp.getWriter().write(jsonString);
    }


}
